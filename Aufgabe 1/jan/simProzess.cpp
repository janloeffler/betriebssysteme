/* 
 * File:   simProzess.cpp
 * Author: debian
 * 
 * Created on 23. April 2017, 15:14
 */

#include "simProzess.h"
int simProzess::counter = 0;
simProzess::simProzess() {
    status = aktiv;
    id=counter++;
    gr0=0;
    gpc=0;
    pid=0;
    
}

void simProzess::SetPid(int pid) {
    this->pid = pid;
}

int simProzess::GetPid() const {
    return pid;
}

simProzess::simProzess(const simProzess& orig) {
}

simProzess::~simProzess() {
}

void simProzess::SetGpc(int gpc) {
    this->gpc = gpc;
}

int simProzess::GetGpc() const {
    return gpc;
}

void simProzess::SetGr0(int gr0) {
    this->gr0 = gr0;
}

int simProzess::GetGr0() const {
    return gr0;
}

void simProzess::SetStatus(int status) {
    this->status = status;
}

int simProzess::GetStatus() const {
    return status;
}

int simProzess::GetId() const {
    return id;
}

void simProzess::SetDateiname(string dateiname) {
    this->dateiname = dateiname;
}

string simProzess::GetDateiname() const {
    return dateiname;
}

